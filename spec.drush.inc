<?php

/**
 * @file
 * Drush integration for the Spec module.
 */

/**
 * Implements hook_drush_command().
 */
function spec_drush_command() {
  $items['spec-run'] = array(
    'description' => dt('Run specs.'),
    'arguments' => array(
      'targets' => 'A test class, a test group. If omitted, a list of test classes and test groups is presented. Delimit multiple targets using commas.',
    ),
    'options' => array(
      'all' => 'Run all available tests',
      'scenarios' => 'A comma delimited list of methods that should be run within the test class. Defaults to all methods.',
    ),
    'aliases' => array('sr'),
  );
  return $items;
}

/**
 * A command callback.
 */
function drush_spec_run($feature) {
  
  $class = _spec_to_camel_case($feature) .'TestCase';
  if (!class_exists($class)) {
    drush_log(t("The feature @class spec doesn't exist.", array('@class' => $class)), 'error');
    return;
  }

  $test = new $class();
  
  $info = $test->getInfo();

  $scenarios = array();
  if ($scenarios_string = drush_get_option('scenarios')) {
    foreach (explode(',', $scenarios_string) as $scenario) {
      $scenarios[] = trim($scenario);
    }
  }

  drush_print(dt('Feature: '. $feature));
  drush_print(dt(_spec_indent(1) . $info['description'] ."\n"));
  
  $spec_log = spec_run($feature, $scenarios);
  
  // Scenarios
  foreach ($spec_log as $scenario_method => $scenario) {
    $scenario_string = preg_replace('/^test/', '', $scenario_method);
    $scenario_string = strtolower(trim(preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $scenario_string)));
    drush_print(_spec_indent(1) ."Scenario: ". $scenario_string);       
    
    // Scenario Examples
    foreach ($scenario['examples'] as $number => $example) {
      
      // Steps
      foreach ($example['steps'] as $step_text => $step) {
        if ($step['status'] == 'fail') {
          drush_print("\033[31m". _spec_indent(2) .$step_text ."\033[0m"); 
        }
        else {
          drush_print(_spec_indent(2) . $step_text);
        }        
        
        // Scripts
        foreach ($step['scripts'] as $sript_method => $script) {
          if ($step['status'] == 'fail') {
            drush_print("\033[31m". _spec_indent(3) .dt($sript_method) ."\033[0m"); 
          }
          else {
            drush_print(_spec_indent(3) . $sript_method);
          }
          
          // Logs
          foreach ($script['logs'] as $i => $log) {
            if ($log['status'] == 'fail') {
              drush_log("\033[31m". _spec_indent(4) . dt($log['message']) ."\033[0m", 'error'); 
            }
            else {
              drush_log(_spec_indent(4) . dt($log['message'])); 
            }
          }          
        }
      }
      
      // New line after each scenario example
      drush_print();
    }
  } 
}

/**
 * Helper functions
 */
function _spec_indent($size) {
  $indent = "";
  for ($i=0; $i < $size; $i++) { 
    $indent .= "  ";
  }

  return $indent;
}
