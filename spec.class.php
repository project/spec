<?php
/**
 * Base class for Spec tests
 */
class SpecTestCase extends DrupalWebTestCase {
  /**
   * Run specs
   *
   * @param Array $scenarios scenarios to run 
   */
  public function runSpecs(array $scenarios = array()) {
    // Initialize verbose debugging.
    simpletest_verbose(NULL, variable_get('file_public_path', conf_path() . '/files'), get_class($this));

    // HTTP auth settings (<username>:<password>) for the simpletest browser
    // when sending requests to the test site.
    $this->httpauth_method = variable_get('simpletest_httpauth_method', CURLAUTH_BASIC);
    $username = variable_get('simpletest_httpauth_username', NULL);
    $password = variable_get('simpletest_httpauth_password', NULL);
    if ($username && $password) {
      $this->httpauth_credentials = $username . ':' . $password;
    }

    set_error_handler(array($this, 'specErrorHandler'));
    $class = get_class($this);
    // Iterate through all the scenarios in this class, unless a specific list of
    // scenarios to run was passed.
    $class_methods = get_class_methods($class);
    if ($scenarios) {
      $class_methods = array_intersect($class_methods, $scenarios);
    }
    
    foreach ($class_methods as $method) {
      // If the current method starts with "test", run it - it's a test.
      if (strtolower(substr($method, 0, 4)) == 'test') {
        // Insert a fail record. This will be deleted on completion to ensure
        // that testing completed.
        $method_info = new ReflectionMethod($class, $method);
        $caller = array(
        'file' => $method_info->getFileName(),
        'line' => $method_info->getStartLine(),
        'function' => $class . '->' . $method . '()',
        );
        $completion_check_id = DrupalTestCase::insertAssert($this->testId, $class, FALSE, t('The test did not complete due to a fatal error.'), 'Completion check', $caller);
        $this->setUp();
        try {
          $this->$method();
        // Finish up.
        }
        catch (Exception $e) {
          $this->specExceptionHandler($e);
        }
        $this->tearDown();
        // Remove the completion check record.
        DrupalTestCase::deleteAssert($completion_check_id);
      }
    }
    // Clear out the error messages and restore error handler.
    drupal_get_messages();
    restore_error_handler();
  }
 
  /**
   * Custom implementation of assert method.
   * Logs each assertion internally so the spec runner can tell
   * which step and which script has passed or failed.
   *
   * @param DrupalTestCase::assert()
   */
  protected function assert($status, $message = '', $group = 'Other', array $caller = NULL) {
    // Convert boolean status to string status.
    if (is_bool($status)) {
      $status = $status ? 'pass' : 'fail';
    }
  
    $this->logStep($status, $message);
    return parent::assert($status, $message, $group, $caller);
  } 
   
  /**
   * Find all Gherkin steps (Given, When, Then, And, But) in a scenario.
   *
   * @param String $class name of Test class
   * @param String $method name of scenario method
   * @return Array of scenario steps ordererd by line number
   */
  public function getSteps($class, $method) {
    $test_method = new ReflectionMethod($class, $method);
    $filename = $test_method->getFileName();
    $start_line = $test_method->getStartLine();
    $end_line = $test_method->getEndLine();
    $source = file($filename);

    $steps = array();
    foreach ($source as $i => $line) {
      $i++;
      if ($i >= $start_line && $i <= $end_line && substr(trim($line), 0, 2) == '//') {
        $line = trim(substr(trim($line), 2));
        if (preg_match('/^[GIVEN|WHEN|THEN|AND|BUT]+/i', $line)) {
          $steps[$i] = $line;
        }
      }
    }

    return $steps;
  } 
 
  public $specLog = array();
  
  /**
   * Log a scenario step internally
   *
   * @param String $status value is 'pass' or 'fail'
   * @param String $message the log message
   */
  private function logStep($status, $message = '') {
    // Find script and scenario method
    $script_method = $this->findScriptMethod();

    if (!$script_method) {
      return;
    }

    $srm = $script_method['function'];
    $scenario_method = $sm = $script_method['parent_function'];
    $scenario_example_number = $sem = is_null($this->currentOutlineCounter) ? 0 : $this->currentOutlineCounter;
    $scenario_example =  array(
      'number' => $scenario_example_number,
      'params' => is_null($this->currentOutline) ? array() : $this->currentOutline,
    );

    // Find the previous step for this method
    $steps = $this->getSteps($script_method['parent_class'], $scenario_method);
    $step = $this->findStep($steps, $script_method['line']);

    // Parse the step text
    $step_text = $st = $this->parseStepText($step['text']);
    
    $log = array(
      'line' => $step['line'],
      'text' => $step_text,
      'message' => $message,
      'scenario_method' => $scenario_method,
      'scenario_example' => $scenario_example,
      'script' => $srm,
      'status' => $status,
    );
    
    // Store as multidimensional array:
    // scenarios -> examples -> steps -> scripts -> logs
    
    if (!isset($this->specLog[$sm])) {
      $this->specLog[$sm] = array(
        'status' => 'pass',
        'examples' => array(),
      );
    }
    
    if (!isset($this->specLog[$sm]['examples'][$sem])) {
      $this->specLog[$sm]['examples'][$sem] = array(
        'status' => 'pass',
        'steps' => array(),
      );
    }

    if (!isset($this->specLog[$sm]['examples'][$sem]['steps'][$st])) {
      $this->specLog[$sm]['examples'][$sem]['steps'][$st] = array(
        'status' => 'pass',
        'scripts' => array(),
      );
    }
    
    if (!isset($this->specLog[$sm]['examples'][$sem]['steps'][$st]['scripts'][$srm])) {
      $this->specLog[$sm]['examples'][$sem]['steps'][$st]['script'][$srm] = array(
        'status' => 'pass',
        'logs' => array(),
      );
    }    
  
    $this->specLog[$sm]['examples'][$sem]['steps'][$st]['scripts'][$srm]['logs'][] = $log;
    
    if ($status == 'fail') {
      $this->specLog[$sm]['status'] = 
      $this->specLog[$sm]['examples'][$sem]['status'] = 
      $this->specLog[$sm]['examples'][$sem]['steps'][$st]['status'] = 
      $this->specLog[$sm]['examples'][$sem]['steps'][$st]['scripts'][$srm]['status'] = 'fail';
    }
    
    // TODO implement realtime logging
    // drush_print($message);
    //
    // We must wait for all logs of a script to finish before we
    // can mark a script as passed/failed. Likewise, we must wait
    // for all scripts to finish before we mark a step passed/failed.
  }
  
  /**
   * Parse a scenario step and replace placeholders with
   * params from the current outline example.
   *
   * @param String $text scenario step text
   * @return String parsed scenario step text
   */
  private function parseStepText($text) {
    if (!is_null($this->currentOutline)) {
      // Make params available as variables for t() function
      $vars = array();
      foreach ($this->currentOutline as $name => $param) {
        // Only params that are strings
        if (!is_string($param)) {
          continue;
        }

        // @see format_string()
        $vars['!'. $name] = $vars['@'. $name] = $vars['%'. $name] = $param;
      }
      
      $text = t($text, $vars);
    }

    return $text;
  }

  /**
   * Find the current script method
   *
   * @return Array the script method
   */
  private function findScriptMethod() {
    $method = NULL;
    $backtrace = debug_backtrace();
    foreach ($backtrace as $i => $_caller) {
      if (strtolower(substr($_caller['function'], 0, 4)) == 'test') {
        $method['parent_class'] = $_caller['class'];      
        $method['parent_function'] = $_caller['function'];
        break;
      }
      $method = $_caller;
    }
    
    if (!isset($method['parent_class'])) {
      return NULL;
    }

    return $method;
  }

  /**
   * Find a step by line number
   *
   * @param Array $steps
   * @param Integer $line line number
   * @return Array step text, line number
   */
  private function findStep($steps, $line) {
    for($i = $line; $i>=0; $i--) {
      if (isset($steps[$i])) {
        return array('text' => $steps[$i], 'line' => $i);
      }
    }

    return NULL;
  }
  
  private $currentOutlineArray = NULL;
  private $currentOutlineCounter = NULL;  
  private $currentOutline = NULL;
  
  /**
   * Run the next example in a scenario outline until all
   * examples have been run.
   * 
   *   Example:
   *
   *   while ($params = $this->outline(array(
   *     array('user' => 'max', 'color' => 'red'), 
   *     array('user' => 'jim', 'color' => 'green'),
   *   ))) {
   *     // Given I am @user
   *     // Then my favorite color is @color
   *   }
   *
   * @param Array outline_array an ordered array of outline examples
   * @return Array current outline example
   */
  protected function outline($outline_array) {
    if (is_null($this->currentOutlineArray)) {
      $this->currentOutlineArray = $outline_array;
      $this->currentOutlineCounter = 1;
    }
    else if (count($this->currentOutlineArray) == 0) {
      // All examples have run so terminate the loop
      $this->currentOutlineArray = $this->currentOutlineCounter = $this->currentOutline = NULL;
      return FALSE;
    }
    
    $this->currentOutline = array_shift($this->currentOutlineArray);
    $this->currentOutlineCounter++;
    
    return $this->currentOutline;
  }  
 
  /**
  * Handle errors during test runs.
  *
  * Because this is registered in set_error_handler(), it has to be public.
  * @see set_error_handler
  */
  public function specErrorHandler($severity, $message, $file = NULL, $line = NULL) {
    if ($severity & error_reporting()) {
      $error_map = array(
        E_STRICT => 'Run-time notice',
        E_WARNING => 'Warning',
        E_NOTICE => 'Notice',
        E_CORE_ERROR => 'Core error',
        E_CORE_WARNING => 'Core warning',
        E_USER_ERROR => 'User error',
        E_USER_WARNING => 'User warning',
        E_USER_NOTICE => 'User notice',
        E_RECOVERABLE_ERROR => 'Recoverable error',
      );

      $backtrace = debug_backtrace();
      $caller = _drupal_get_last_caller($backtrace);

      $this->error($message, $error_map[$severity], $caller);
      $this->logStep('fail', 'Error: '. $message ." (in ". $caller['file'] .", line ". $caller['line'] .")");
    }
    return TRUE;
  }

  /**
   * Handle exceptions.
   *
   * @see set_exception_handler
   */
  protected function specExceptionHandler($exception) {
    $backtrace = $exception->getTrace();
    // Push on top of the backtrace the call that generated the exception.
    array_unshift($backtrace, array(
     'line' => $exception->getLine(),
     'file' => $exception->getFile(),
    ));
    require_once DRUPAL_ROOT . '/includes/errors.inc';
    // The exception message is run through check_plain() by _drupal_decode_exception().
    $message = t('%type: !message in %function (line %line of %file).', _drupal_decode_exception($exception));
    $this->error($message, 'Uncaught exception', _drupal_get_last_caller($backtrace));

    $this->logStep('pass', 'Uncaught exception: '. $message);
  }
  
  /**
   * SimpleTest Fixture methods
   *
   * Copied from simpletest_fixture_test_case.php
   */
   
  /**
   * Set up environment (with fixtures) to run a test
   */
  protected function setUpFixture() {
    // copied from DrupalWebTestCase::setup() and modified
    
    global $user, $language, $conf;
  
    // How this works:
    // - there needs to be a valid prefix like 'simpletest123456',
    // - the prefix will get passed along with the user agent header information
    // - Drupal will look for 'simpletest..' in the header and use it as db prefix
    //   for requests during testing (SimpleTest & Selenium)
    // => fixture db tables must all be prefixed too!
    
    // Retrieve the static prefix parameter stored in the fixture db info in settings.php
    $fixturedb_connection = Database::getConnectionInfo('fixture');
    $this->databasePrefix = $fixturedb_connection['default']['simpletest_prefix'];

    db_update('simpletest_test_id')
      ->fields(array('last_prefix' => $this->databasePrefix))
      ->condition('test_id', $this->testId)
      ->execute();
    
    // Prepare switch to fixture database
    Database::renameConnection('default', 'simpletest_original_default');
    foreach ($fixturedb_connection as $target => $value) {
      $fixturedb_connection[$target]['prefix'] = array(
        'default' => $value['prefix']['default'] . $this->databasePrefix,
      );
    }    
    Database::addConnectionInfo('default', 'default', $fixturedb_connection['default']);
    
    // Store necessary current values before switching to the fixture database.
    $this->originalLanguage = $language;
    $this->originalLanguageDefault = variable_get('language_default');
    $this->originalFileDirectory = variable_get('file_public_path', conf_path() . '/files');
    $this->originalProfile = drupal_get_profile();
    $this->removeTables = variable_get('simpletest_remove_tables', TRUE);
    $clean_url_original = variable_get('clean_url', 0);

    // Save and clean shutdown callbacks array because it static cached and
    // will be changed by the test run. If we don't, then it will contain
    // callbacks from both environments. So testing environment will try
    // to call handlers from original environment.
    $callbacks = &drupal_register_shutdown_function();
    $this->originalShutdownCallbacks = $callbacks;
    $callbacks = array();

    // Create test directory ahead of installation so fatal errors and debug
    // information can be logged during installation process.
    // Use temporary files directory with the same prefix as the database.
    $public_files_directory  = $this->originalFileDirectory . '/simpletest/' . substr($this->databasePrefix, 10);
    $private_files_directory = $public_files_directory . '/private';
    $temp_files_directory    = $private_files_directory . '/temp';

    // Create the directories
    file_prepare_directory($public_files_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    file_prepare_directory($private_files_directory, FILE_CREATE_DIRECTORY);
    file_prepare_directory($temp_files_directory, FILE_CREATE_DIRECTORY);
    $this->generatedTestFiles = FALSE;

    // Log fatal errors.
    ini_set('log_errors', 1);
    ini_set('error_log', $public_files_directory . '/error.log');

    // Reset all statics and variables to perform tests in a clean environment.
    $conf = array();
    drupal_static_reset();

    // Set the test information for use in other parts of Drupal.
    $test_info = &$GLOBALS['drupal_test_info'];
    $test_info['test_run_id'] = $this->databasePrefix;
    $test_info['in_child_site'] = FALSE;

    // $this->setUpInstall(func_get_args(), $public_files_directory, $private_files_directory, $temp_files_directory);

    // Rebuild caches.
    // TODO time-consuming. Do we need this?
    // drupal_static_reset();
    // drupal_flush_all_caches();

    // Register actions declared by any modules.
    actions_synchronize();

    // Reload global $conf array and permissions.
    $this->refreshVariables();
    $this->checkPermissions(array(), TRUE);

    // Reset statically cached schema for new database prefix.
    drupal_get_schema(NULL, TRUE);

    // Run cron once in that environment, as install.php does at the end of
    // the installation process.
    // TODO time-consuming. Do we need this?
    // drupal_cron_run();

    // Log in with a clean $user.
    $this->originalUser = $user;
    drupal_save_session(FALSE);
    $user = user_load(1);
    
    // TODO time-consuming. Do we need this?
    // $this->setUpVariables($clean_url_original);

    // Set up English language.
    unset($GLOBALS['conf']['language_default']);
    $language = language_default();

    // Use the test mail class instead of the default mail handler class.
    variable_set('mail_system', array('default-system' => 'TestingMailSystem'));

    drupal_set_time_limit($this->timeLimit);
  }

  /**
   * Delete created files and temporary files directory
   * and reset the database prefix.
   */
  protected function tearDownFixture() {
    // copied from DrupalWebTestCase::tearDown() and modified
    global $user, $language;

    // In case a fatal error occured that was not in the test process read the
    // log to pick up any fatal errors.
    simpletest_log_read($this->testId, $this->databasePrefix, get_class($this), TRUE);

    $emailCount = count(variable_get('drupal_test_email_collector', array()));
    if ($emailCount) {
      $message = format_plural($emailCount, '1 e-mail was sent during this test.', '@count e-mails were sent during this test.');
      $this->pass($message, t('E-mail'));
    }

    // Delete temporary files directory.
    file_unmanaged_delete_recursive($this->originalFileDirectory . '/simpletest/' . substr($this->databasePrefix, 10));

    // Get back to the original connection.
    Database::removeConnection('default');
    Database::renameConnection('simpletest_original_default', 'default');

    // Restore original shutdown callbacks array to prevent original
    // environment of calling handlers from test run.
    $callbacks = &drupal_register_shutdown_function();
    $callbacks = $this->originalShutdownCallbacks;

    // Return the user to the original one.
    $user = $this->originalUser;
    drupal_save_session(TRUE);

    // Ensure that internal logged in variable and cURL options are reset.
    $this->loggedInUser = FALSE;
    $this->additionalCurlOptions = array();

    // Reload module list and implementations to ensure that test module hooks
    // aren't called after tests.
    module_list(TRUE);
    module_implements('', FALSE, TRUE);

    // Reset the Field API.
    field_cache_clear();

    // Rebuild caches.
    $this->refreshVariables();

    // Reset language.
    $language = $this->originalLanguage;
    if ($this->originalLanguageDefault) {
      $GLOBALS['conf']['language_default'] = $this->originalLanguageDefault;
    }

    // Close the CURL handler.
    $this->curlClose();
  }
}